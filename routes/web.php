<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Route::get('/empleados', function () {
    return view('empleado');
})->middleware('auth');


Route::get('/entregas', function () {
    return view('entrega');
})->middleware('auth');


Route::get('/entregas/consulta', function () {
    return view('consulta_entrega');
})->middleware('auth');


Route::get('/logout', function () {
    Auth::logout();
    return redirect('login');
})->middleware('auth');











Auth::routes();


