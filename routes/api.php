<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login','App\Http\Controllers\LoginController@authenticate');
Route::post('/register','App\Http\Controllers\RegisterController@store');
Route::get('/empleados','App\Http\Controllers\EmpleadoController@index');

Route::get('/empleados','App\Http\Controllers\EmpleadoController@index');
Route::get('/empleados/{id}','App\Http\Controllers\EmpleadoController@show');
Route::put('/empleados/{id}','App\Http\Controllers\EmpleadoController@update');
Route::post('/empleados','App\Http\Controllers\EmpleadoController@store');
Route::delete('/empleados/{id}','App\Http\Controllers\EmpleadoController@destroy');

Route::get('/empleados/{id}/sueldo','App\Http\Controllers\EmpleadoController@sueldoEmpleado');

Route::get('/entregas','App\Http\Controllers\EntregaController@index');
Route::get('/entregas/{id}','App\Http\Controllers\EntregaController@show');
Route::put('/entregas/{id}','App\Http\Controllers\EntregaController@update');
Route::post('/entregas','App\Http\Controllers\EntregaController@store');
Route::delete('/entregas/{id}','App\Http\Controllers\EntregaController@destroy');
Route::get('/entregas/{id}/movimientos','App\Http\Controllers\EntregaController@movimientos');
Route::get('/entregas/{id}/movimiento','App\Http\Controllers\EntregaController@movimiento');;

Route::get('/empleado/tipo','App\Http\Controllers\TipoController@index'); 
Route::get('/empleado/rol','App\Http\Controllers\RolController@index'); 

Route::get('/logout','App\Http\Controllers\LoginController@logout')->middleware('auth:api'); ;



