<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entrega;
use App\Bonos\Bono;

class Empleado extends Model
{
    use HasFactory;
    protected $primaryKey="empleado"; //indica la llave primaria
    //constantes
    const JORNADA_LABORAL = 8;
    const BONO_ENTREGA = 5;
    const RETENCION_ISR = 0.09;
    const RETENCION_ADICIONAL = 0.03;
    const VALE_DESPENSA = 0.04;
 


    protected $fillable = [
        'nombre1',
        'nombre2',
        'apellido_paterno',
        'apellido_materno',
        'apellido_paterno',
        'rol',
        'tipo_empleado',
    ];
    //inserta un nuevo empleado
    public function crearEmpleado($array)
    {
      $empleado=Empleado::create([
          'nombre1' => $array['nombre1'],
          'nombre2' => $array['nombre2'],
          'apellido_paterno' => $array['apellido_paterno'],
          'apellido_materno' => $array['apellido_materno'],
          'rol' => $array['rol'],
          'tipo_empleado' => $array['tipo_empleado'],
      ]);
      return Response()->json(['No Content',204]);
    }
    //muestra todos los empleado
    public function buscarEmpleados()
    {
        $empleado=Empleado::select('empleado',
                                    'nombre1',
                                    'nombre2',
                                    'apellido_paterno',
                                    'apellido_materno',
                                    'rol_empleado',
                                    'tipo_empleados.tipo_empleado',
                                    'sueldo_base'
                                    )->
        leftjoin('rol_empleados','rol','rol_empleados.id')->
        leftjoin('tipo_empleados','empleados.tipo_empleado','tipo_empleados.id')->get();
         return $empleado;
    }



    //busca un empleado en especifico
    public function buscarEmpleado($id)
    {
        $empleado=Empleado::select('empleado',
                                    'nombre1',
                                    'nombre2',
                                    'apellido_paterno',
                                    'apellido_materno',
                                    'rol_empleado',
                                    'rol',
                                    'empleados.tipo_empleado',
                                    'tipo_empleados.tipo_empleado',
                                    'sueldo_base'
                                    )->
        leftjoin('rol_empleados','rol','rol_empleados.id')->
        leftjoin('tipo_empleados','empleados.tipo_empleado','tipo_empleados.id')->
        where('empleado',$id)->first();
         return $empleado;
    }
    //actualiza los datos del empleado
    public function actualizarEmpleado($id,$array)
    {
        $empleado=Empleado::buscarEmpleado($id);
        $empleado->update($array);
        return $empleado;
    }
    //elimina a un empleado
    public function eliminarEmpleado($id)
    {
        $empleado=Empleado::buscarEmpleado($id);
        $empleado->delete();
        return  Response()->json(['success',true]);
    }

   //Calcula el sueldo mensual del empleado
    public function calcularSueldo($id)
    {
        $dias= date('t'); //obtiene el numero de dias del mes
        $empleado = Empleado::buscarEmpleado($id); //
        $rol = $empleado->rol; // obtiene el valor del campo rol
        $tipo_empleado = $empleado->tipo_empleado;
        $entregas = Entrega::contarEntregas($id); //obtiene el numero de entregas
        $bono=self::calcularBono($rol,$dias);  
        if ($rol == 3)
        {
            $suplidos = Entrega::buscarSuplente($id); //busca al empleado que tuvo que ser suplido
            foreach($suplidos as $suplido)            //obtiene las entregas que empleado suplio
            {
                $suplido = self::buscarEmpleado($suplido->empleado_suplido);//1
                $dia = Entrega::diaSuplido($id);
                $bono = $bono + Empleado::calcularBono($suplido->rol,$dia); // obtiene el bono del empleado
            }    
                                                             //que tuvo que ser suplido
        }//
   

 
        $sueldo_bruto = ($empleado->sueldo_base * self::JORNADA_LABORAL) * $dias; 
        $sueldo_bruto = $sueldo_bruto + ($entregas * self::BONO_ENTREGA) + $bono; // 240 + (1 * 5) + 80
        $sueldo_bruto =  self::calculaVales($sueldo_bruto,$tipo_empleado);
        $sueldo_neto =   self::calcularISR($sueldo_bruto);
        return  response()->json(['sueldo_neto' => $sueldo_neto,
        'sueldo_bruto'=> $sueldo_bruto,'bono' => $bono , 'isr' => $sueldo_bruto-$sueldo_neto
        ,'entregas'=>$entregas]);   //330
    }

    //Genera un bono por hora segun el
    // tipo de empleado al mes
    public function calcularBono($tipo,$dias)
    {
        return Bono::calcularBono($tipo,$dias);
     
    }
    //calcula ISR
    public function calcularISR($sueldo)
    {
        $sueldo = $sueldo - ($sueldo * self::RETENCION_ISR);
        if($sueldo > 16000)
        {
            $sueldo = $sueldo - ($sueldo * self::RETENCION_ADICIONAL);
        }
        return $sueldo;
    }
    //agrega vales de despensa al sueldo
    public function calculaVales($sueldo,$tipo)
    {
        if ($tipo == 1) // Es interno
        {
            $sueldo = $sueldo + ($sueldo * self::VALE_DESPENSA);
        }
        return $sueldo;
    }




   


}
