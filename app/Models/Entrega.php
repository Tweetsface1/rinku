<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Entrega extends Model
{
    use HasFactory;


    protected $fillable = [
        'empleado',
        'cantidad_entregas',
        'fecha_entrega',
        'empleado_suplido'
    ];

    public function crearEntrega($array)
    {
      $entrega=Entrega::create([
          'empleado' => $array['empleado'],
          'cantidad_entregas'=> $array['cantidad_entregas'],
          'fecha_entrega' => date('Y-m-d'),
          'empleado_suplido' => $array['empleado_suplido'],
      ]);
      return Response()->json(['No Content',204]);
    }

    //muestra todas las entregas
    public function buscarEntregas()
    {
        return Entrega::all();
    }
    //busca una entrega en especifico
    public function buscarEntrega($id)
    {
       return Entrega::findOrFail($id);
    }
    //actualiza los datos de la  entrega
    public function actualizarEntrega($id,$array)
    {
        $empleado=Entrega::buscarEntrega($id);
        $empleado->update($array);
        return $empleado;
    }
    //elimina una entrega
    public function eliminarEntrega($id)
    {
        $empleado=Entrega::buscarEntrega($id);
        $empleado->delete();
        return  Response()->json(['success',true]);
    }

    public function contarEntregas($id)
    {
        $entregas = Entrega::where('empleado',$id);
        $entregas->whereBetween('fecha_entrega',
        [date('Y-m-01'),date('Y-m-t')]);
        return $entregas->sum('cantidad_entregas');
    }

    public function diaSuplido($id)
    {
        $entregas = Entrega::select('fecha_entrega',
        DB::raw('COUNT(distinct(fecha_entrega)) as cantidad'))
        ->where('empleado',$id)->whereBetween('fecha_entrega',
        [date('Y-m-01'),date('Y-m-t')])
        ->groupBy('fecha_entrega')->first()->cantidad;
        return $entregas;
    }

    public function buscarSuplente($id)
    {
        $entregas=Entrega::where('empleado',$id);
        $entregas->whereBetween('fecha_entrega',[date('Y-m-01'),date('Y-m-t')]);
       return $entregas->get();
    }

    public function buscarMovimientos($id)
    {
        $entregas=Entrega::select('entregas.id','entregas.empleado',
        'nombre1','nombre2','apellido_paterno','apellido_materno',
        'cantidad_entregas','empleado_suplido','fecha_entrega'
         )->leftjoin('empleados','empleados.empleado',
         'entregas.empleado')->where('empleados.empleado',
         $id)->orderBy('fecha_entrega','desc')->get();
        return  $entregas;
    }

    public function buscarMovimiento($id)
    {
        $entregas=Entrega::select('entregas.id','entregas.empleado',
        'nombre1','nombre2','apellido_paterno','apellido_materno',
        'cantidad_entregas','empleado_suplido','fecha_entrega'
         )->leftjoin('empleados','empleados.empleado',
         'entregas.empleado')->where('entregas.id',$id)->
         orderBy('fecha_entrega','desc')->first();
        return  $entregas;
    }

  

}
