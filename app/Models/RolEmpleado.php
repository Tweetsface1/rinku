<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolEmpleado extends Model
{
    use HasFactory;
    //obtiene los roles que puede ocupar un empleado
    public function obtieneRol()
    {
        return self::all();
    }
}
