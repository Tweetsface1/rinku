<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BonoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('bono', function()
        {
            return new \App\Bonos\Facades\Bono;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
