<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Bono extends Facade {

    protected static function getFacadeAccessor()
    {        return 'bono';
        
    }
}