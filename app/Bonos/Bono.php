<?php
namespace App\Bonos;
use App\Models\Empleado;

class Bono {

    public function calcularBono($tipo,$dias)
    { 
         switch($tipo)
        {
            case 1:
              $bono = (10 * Empleado::JORNADA_LABORAL) * $dias;
              break;
            case 2:
              $bono = (5 *  Empleado::JORNADA_LABORAL) * $dias;
              break;
            default:
             $bono = 0 ;
        }
        return $bono;
    }
}