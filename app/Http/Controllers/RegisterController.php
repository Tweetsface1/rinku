<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class RegisterController extends Controller
{
    public function store(Request $request)
    {
        $user = new User();
        $user::crearUsuario($request->all());
        return Response()->json(['No-Content'=>201]);
    }
}
