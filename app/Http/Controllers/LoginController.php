<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
   
    public function authenticate(Request $request)
    {
    $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials))
        {
            return response()->json(['error' => 'invalid_credentials'], 400);
        }
            $accessToken=Auth::User()->createToken('token')->accessToken;
            return Response()->json(['token'=>$accessToken]);
    }


     public  function logout()
     {
         return Auth::logout();
     }
     
 

}
