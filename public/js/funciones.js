//-- 90120580 MIGUEL ANGEL ESPINOZA SALMORAN
// Las funciones dentro de este documento permiten consumir a la api Rinku
//

function CargarTabla(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    ,
      }
     }); 
     $.ajax({
      url:"/api/empleados",
      async:false,
      type:'get',
      dataType:"json",
      success:function(data)
      {   
        $("#cuerpo").html("");
          for(var i=0; i<data.length; i++){
           var id=data[i]["empleado"]
           var nombre1=data[i]["nombre1"];
           var nombre2=data[i]["nombre2"];
           var apellidop=data[i]["apellido_paterno"];
           var apellidom=data[i]["apellido_materno"];
           var tipo=data[i]["tipo_empleado"];
           var tipo_empleado;
           if (tipo=="Externo" || tipo=="EXTERNO" || tipo=="externo")
           {
             tipo_empleado=`<span class="badge bg-primary text-white">`+tipo+`</span>`;
           }else
           {
            tipo_empleado=`<span class="badge bg-warning text-white">`+tipo+`</span>`;
           }
            var tr = `<tr>
            <td>`+id+`</td>
              <td>`+nombre1+' '+nombre2+' '+apellidop+ ' ' +apellidom+`</td>
              <td>`+data[i]["rol_empleado"]+`</td>
              <td>`+tipo_empleado+`</td>
              <td>`+`<span class="badge bg-info text-white">`+data[i]["sueldo_base"]+`</span>`+`</td>
               <td>`+`<a href id="btnEditar" data-toggle="modal" data-target="#exampleModal2" class="btn btn-outline-light" onClick="consultaEmpleado(${id});" value="${id}"><img src="img/escritura.png" alt="pago" width="30"></a>
               <button   id="btnDelete" onClick="fnDelete(${id});"  class="btn btn-outline-light" ><img src="img/eliminar.png" alt="pago" width="30"></button>
               <button id="btnNomina" onClick="generarPDF(${id});"  class="btn btn-outline-light" ><img src="img/pago.png" alt="pago" width="30"></button>`
               
               + `</td>
            </tr>`;
            $("#cuerpo").append(tr)
          }
      },
       error:function(x,xs,xt){
      
               console.log('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
            }
  }); 
}

function consultaRoles(){
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
    }
   }); 
    $.ajax({
    url:"/api/empleado/rol",
    type:'get',
    dataType:"json",
    success:function(data){
    var id,rol;
    for(var i=0;i<data.length;i++){
      id=data[i]["id"];
      rol=data[i]["rol_empleado"];
      var tr=`<option value="`+id+`">`+rol+`</option>`
      $("#rol").append(tr)
      $("#rol_edit").append(tr)
    }
    },
     error:function(x,xs,xt){
    
              alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
        })
    }

    function consultaTipoEmpleados(){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
        }
       }); 
        $.ajax({
        url:"/api/empleado/tipo",
        type:'get',
        dataType:"json",
        success:function(data){
        var id,tipo;
        for(var i=0;i<data.length;i++){
          id=data[i]["id"];
          tipo=data[i]["tipo_empleado"];
          var tr=`<option value="`+id+`">`+tipo+`</option>`
          $("#tipo_empleado").append(tr)
          $("#tipo_empleado_edit").append(tr)
        }
        },
         error:function(x,xs,xt){
        
                  alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
              }
            })
        }

        function fnCreate(){
            $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
             }
            }); 
            var nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo;
             nombre1=document.getElementById("nombre1").value;
             nombre2=document.getElementById("nombre2").value;
             apellido_paterno=document.getElementById("apellido_paterno").value;
             apellido_materno=document.getElementById("apellido_materno").value;
             rol=document.getElementById("rol").value;
             tipo_empleado=document.getElementById("tipo_empleado").value;
            if (nombre1   && apellido_materno && rol && tipo_empleado){
            $.ajax({
                  url:"/api/empleado",
                  data:{'nombre1':nombre1,'nombre2':nombre2,'apellido_paterno':apellido_paterno,
                  'apellido_materno':apellido_materno,'rol':rol,'tipo_empleado':tipo_empleado},
                  type:'post',
                  success: function (response) {
                  alert("Datos Guardados Correctamente")
                  window.location.reload();
                  },
                  statusCode: {
                     404: function() {
                        alert('web not found');
                     }
                  },
                  error:function(x,xs,xt){
                    alert('Ha ocurrido un error');
                  }
        });
        }else{
            alert("Ingresa todos los campos")
        }
        }

        function fnDelete(id){
            $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
             }
            }); 
            $.ajax({
                  url:"/api/empleados/"+id,
                  type:'delete',
                  success: function (response) {
                  alert("Se elimino el contacto")
                  window.location.reload();
                  },
                  statusCode: {
                     404: function() {
                        alert('web not found');
                     }
                  },
                  error:function(x,xs,xt){
                    alert('Ha ocurrido un error');
                  }
        });
        }

         function fnUpdate(){
              $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
             }
            }); 
            var empleado,nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo_empleado;
             empleado=document.getElementById("empleado").value;
             nombre1=document.getElementById("nombre1_edit").value;
             nombre2=document.getElementById("nombre2_edit").value;
             apellido_paterno=document.getElementById("apellido_paterno_edit").value;
             apellido_materno=document.getElementById("apellido_materno_edit").value;
             rol=document.getElementById("rol_edit").value;
             tipo_empleado=document.getElementById("tipo_empleado_edit").value;
            if (nombre1 && nombre2 && apellido_paterno && apellido_materno ){
            $.ajax({
                  url:"/api/empleados/"+empleado,
                  data:{'nombre1':nombre1,'nombre2':nombre2,'apellido_paterno':apellido_paterno,
                  'apellido_materno':apellido_materno,'rol':rol,'tipo_empleado':tipo_empleado},
                  type:'put',
                  success: function (response) {
                  alert("Datos Actualizados Correctamente")
                  window.location.reload();
                  },
                  statusCode: {
                     404: function() {
                        alert('web not found');
                     }
                  },
                  error:function(x,xs,xt){
                    alert('Ha ocurrido un error');
                  }
        });
    }
}


        function consultaEmpleado(id){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
                }
               }); 
            $.ajax({
            url:"/api/empleados/"+id,
            type:'get',
            dataType:"json",
            success:function(data)
            {
                var empleado,nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo_empleado;
                empleado=data["empleado"];
                nombre1=data["nombre1"];
                nombre2=data["nombre2"];
                apellido_paterno=data["apellido_paterno"];
                apellido_materno=data["apellido_materno"];
                rol=data["rol"];
                tipo_empleado=data["tipo_empleado"];
                document.getElementById("empleado").value=empleado;
                document.getElementById("nombre1_edit").value=nombre1;
                document.getElementById("nombre2_edit").value=nombre2;
                document.getElementById("apellido_paterno_edit").value=apellido_paterno;
                document.getElementById("apellido_materno_edit").value=apellido_materno;
            },
            statusCode: {
                404: function() {
                   alert('web not found');
                }
             },
             error:function(x,xs,xt){
            
                      alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                  }
                })
            }

    $( document ).ready(function() {
      CargarTabla()
      consultaRoles();
      consultaTipoEmpleados()
      fechaActual()
      });

      function fechaActual()
      {
        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
           dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
        {
           mes='0'+mes //agrega cero si el menor de 10
        }
        document.getElementById("fecha_entregas").value=ano+"-"+mes+"-"+dia;
      }

      $(document).ready(function () {
        $("#buscador").keyup(function (e) {
            if (e.keyCode == 13) {
              var txtBuscador=document.getElementById("buscador").value;
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
                }
               }); 
            $.ajax({
            url:"/api/empleados/"+txtBuscador,
            type:'get',
            dataType:"json",
            success:function(data)
            {
                var empleado,nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo_empleado;
                empleado=data["empleado"];
                nombre1=data["nombre1"];
                nombre2=data["nombre2"];
                apellido_paterno=data["apellido_paterno"];
                apellido_materno=data["apellido_materno"];
                rol=data["rol"];
                tipo_empleado=data["tipo_empleado"];
                 $("#cuerpo").html("");
                var tr = `<tr>
                <td>`+empleado+`</td>
                  <td>`+nombre1+' '+nombre2+' '+apellido_paterno+ ' ' +apellido_materno+`</td>
                  <td>`+data["rol_empleado"]+`</td>
                  <td>`+data["tipo_empleado"]+`</td>
                  <td>`+data["sueldo_base"]+`</td>
                   <td>`+`<a href id="btnEditar" data-toggle="modal" data-target="#exampleModal2" class="btn btn-outline-dark" onClick="consultaEmpleado(${empleado});" value="${empleado}">Editar</a>
                   <button   id="btnDelete" onClick="fnDelete(${empleado});"  class="btn btn-outline-success" >Eliminar</button>`
                   + `</td>
                </tr>`;
                $("#cuerpo").append(tr)
            },
            statusCode: {
                404: function() {
                   alert('web not found');
                }
             },
             error:function(x,xs,xt){
            
                      alert('No se encontro al empleado');
                  }
                })
            }
        });
        
    });


    $(document).ready(function () {
      $("#buscador_suplido").keyup(function (e) {
          if (e.keyCode == 13) {
            var txtBuscador2=document.getElementById("buscador_suplido").value;
            var txtBuscador1=document.getElementById("buscador_empleado").value;
            var txtRol=document.getElementById("rol_oculto").value;
            $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
              }
             }); 
            if(txtRol==3){
          if (txtBuscador2 != txtBuscador1){
          $.ajax({
          url:"/api/empleados/"+txtBuscador2,
          type:'get',
          dataType:"json",
          success:function(data)
          {
              var empleado,nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo_empleado;
              empleado=data["empleado"];
              nombre1=data["nombre1"];
              nombre2=data["nombre2"];
              apellido_paterno=data["apellido_paterno"];
              apellido_materno=data["apellido_materno"];
              rol=data["rol_empleado"];
              tipo_empleado=data["tipo_empleado"];
              document.getElementById("id_suplido").value=empleado;
              document.getElementById("nombre1_suplido").value=nombre1;
              document.getElementById("nombre2_suplido").value=nombre2;
              document.getElementById("apellido_paterno_suplido").value=apellido_paterno;
              document.getElementById("apellido_materno_suplido").value=apellido_materno;
              document.getElementById("rol_suplido").value=rol;
              document.getElementById("tipo_suplido").value=tipo_empleado;
          },
          statusCode: {
              404: function() {
                 alert('web not found');
              }
           },
           error:function(x,xs,xt){
          
                    alert('No se encontro al empleado');
                    document.getElementById("nombre1_suplido").value='';
                    document.getElementById("nombre2_suplido").value='';
                    document.getElementById("apellido_paterno_suplido").value='';
                    document.getElementById("apellido_materno_suplido").value='';
                    document.getElementById("rol_suplido").value='';
                    document.getElementById("tipo_suplido").value='';
                }
              })
            }else{
              alert("El empleado no puede suplirse a si mismo");
            }
             }else{
              alert("Este empleado no puede cubrir turno");
            }
          }
       
      });
  });

  $(document).ready(function () {
    $("#buscador_empleado").keyup(function (e) {
        if (e.keyCode == 13) {
          var txtBuscador=document.getElementById("buscador_empleado").value;
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
            }
           }); 
        $.ajax({
        url:"/api/empleados/"+txtBuscador,
        type:'get',
        dataType:"json",
        success:function(data)
        {
            var empleado,nombre1,nombre2,apellido_paterno,apellido_paterno,rol,tipo_empleado;
            empleado=data["empleado"];
            nombre1=data["nombre1"];
            nombre2=data["nombre2"];
            apellido_paterno=data["apellido_paterno"];
            apellido_materno=data["apellido_materno"];
            rol=data["rol_empleado"];
            rol2=data["rol"]
            tipo_empleado=data["tipo_empleado"];
            document.getElementById("id_entregas").value=empleado;
            document.getElementById("nombre1_entregas").value=nombre1;
            document.getElementById("nombre2_entregas").value=nombre2;
            document.getElementById("apellido_paterno_entregas").value=apellido_paterno;
            document.getElementById("apellido_materno_entregas").value=apellido_materno;
            document.getElementById("rol_entregas").value=rol;
            document.getElementById("rol_oculto").value=rol2;
            document.getElementById("tipo_entregas").value=tipo_empleado;
        },
        statusCode: {
            404: function() {
               alert('web not found');
            }
         },
         error:function(x,xs,xt){
        
                  alert('No se encontro al empleado');
                  document.getElementById("nombre1_entregas").value='';
                  document.getElementById("nombre2_entregas").value='';
                  document.getElementById("apellido_paterno_entregas").value='';
                  document.getElementById("apellido_materno_entregas").value='';
                  document.getElementById("rol_entregas").value='';
                  document.getElementById("tipo_entregas").value='';
              }
            })
        }
    });
});

  

function fnCreateMov(){
  $.ajaxSetup({
   headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
   }
  }); 
  var empleado,entregas,fecha,suplido,nombre1,nombre2;
   empleado=document.getElementById("id_entregas").value;
   entregas=document.getElementById("cantidad_entregas").value;
   suplido=document.getElementById("id_suplido").value;
   nombre1=document.getElementById("nombre1_entregas").value;
   nombre2=document.getElementById("nombre1_suplido").value;
  if (empleado){
    if (entregas){
  $.ajax({
        url:"/api/entregas",
        data:{'empleado':empleado,'cantidad_entregas':entregas,
        'empleado_suplido':suplido},
        type:'post',
        success: function (response) {
        alert("Datos Guardados Correctamente")
        window.location.reload();
        },
        statusCode: {
           404: function() {
              alert('web not found');
           }
        },
        error:function(x,xs,xt){
          alert('Ha ocurrido un error');
        }
});
    }else
    {
      alert("Ingrese el numero de entregas")
    }
}else{
  alert("Ingresa todos los campos")
}
}

function CargarMovimientos(){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    ,
    }
   }); 
   $.ajax({
    url:"/api/entregas/"+id+"/movimientos",
    async:false,
    type:'get',
    dataType:"json",
    success:function(data)
    {   
      $("#entregas").html("");
        for(var i=0; i<data.length; i++){
         var id=data[i]["empleado"]
         var nombre1=data[i]["nombre1"];
         var nombre2=data[i]["nombre2"];
         var apellidop=data[i]["apellido_paterno"];
         var apellidom=data[i]["apellido_materno"];
          var tr = `<tr>
          <td>`+id+`</td>
            <td>`+nombre1+' '+nombre2+' '+apellidop+ ' ' +apellidom+`</td>
            <td>`+data[i]["rol_empleado"]+`</td>
            <td>`+data[i]["tipo_empleado"]+`</td>
            <td>`+data[i]["sueldo_base"]+`</td>
             <td>`+`<a href id="btnEditar" data-toggle="modal" data-target="#exampleModal2" class="btn btn-outline-dark" onClick="consultaEmpleado(${id});" value="${id}">Editar</a>
             <button   id="btnDelete" onClick="fnDelete(${id});"  class="btn btn-outline-success" >Eliminar</button>`
             + `</td>
          </tr>`;
          $("#entregas").append(tr)
        }
    },
     error:function(x,xs,xt){
    
             console.log('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
}); 
}

function fnDeleteEntrega(id){
  $.ajaxSetup({
   headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
   }
  }); 
  $.ajax({
        url:"/api/entregas/"+id,
        type:'delete',
        success: function (response) {
        alert("Movimiento eliminado con exito")
        window.location.reload();
        },
        statusCode: {
           404: function() {
              alert('web not found');
           }
        },
        error:function(x,xs,xt){
          alert('Ha ocurrido un error');
        }
});
}

$(document).ready(function () {
  $("#buscador_movimientos").keyup(function (e) {
      if (e.keyCode == 13) {
        var id=document.getElementById("buscador_movimientos").value;
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
          }
         }); 
      $.ajax({
      url:"/api/entregas/"+id+"/movimientos",
      type:'get',
      dataType:"json",
      success:function(data)
      {
          $("#entregas").html("");
          for(var i=0; i<data.length; i++){
            var id= data[i]["id"];
            var suplido = data[i]["empleado_suplido"];
            var nombre=data[i]["nombre1"]+ " "+data[i]["nombre2"]+" "+
            data[i]["apellido_paterno"]+" " +data[i]["apellido_materno"];
            if ((suplido == null))
            {
              suplido="0";
            }
            var tr = `<tr>
            <td>`+data[i]["empleado"]+`</td>
              <td>`+nombre+`</td>
              <td>`+data[i]["cantidad_entregas"]+`</td>
              <td>`+suplido+`</td>
              <td>`+data[i]["fecha_entrega"]+`</td>
               <td>`+`<a href id="btnEditar" data-toggle="modal" data-target="#exampleModal2" class="btn btn-outline-dark" onClick="consultaMovimiento(${id});" value="${id}">Editar</a>
               <button   id="btnDelete" onClick="fnDeleteEntrega(`+id+`);"  class="btn btn-outline-success" >Eliminar</button>`
               + `</td>
            </tr>`;
            $("#entregas").append(tr);
          }
      },
      statusCode: {
          404: function() {
           
          }
       },
       error:function(x,xs,xt){
                alert('No se encontro al empleado');
            }
          })
      }
  });
});

function consultaMovimiento(id)
{
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
    }
   }); 
$.ajax({
url:"/api/entregas/"+id+"/movimiento",
type:'get',
dataType:"json",
success:function(data)
{
var cantidad,ide
ide=data["id"];
cantidad=data["cantidad_entregas"];
document.getElementById('cantidad').value=cantidad;
document.getElementById('id_entrega').value=ide;
    
},
statusCode: {
    404: function() {
       alert('web not found');
    }
 },
 error:function(x,xs,xt){

          alert('No se actualizo el campo');
      }
    })
}

function fnUpdateCantidad(){
  $.ajaxSetup({
 headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
 }
}); 
var cantidad,id;
 cantidad=document.getElementById("cantidad").value;
 id=document.getElementById("id_entrega").value;
if (cantidad){
$.ajax({
      url:"/api/entregas/"+id,
      data:{'cantidad_entregas':cantidad},
      type:'put',
      success: function (response) {
      alert("Datos Actualizados Correctamente")
      window.location.reload();
      },
      statusCode: {
         404: function() {
            alert('web not found');
         }
      },
      error:function(x,xs,xt){
        alert('Ha ocurrido un error');
      }
});
}
}


function fnlogout(){
  $.ajaxSetup({
   headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
   }
  }); 
  $.ajax({
        url:"/api/logout",

        type:'get',
        success: function (response) {
        window.location.reload();
        },
        statusCode: {
           404: function() {
              alert('web not found');
           }
        },
        error:function(x,xs,xt){
          alert('Ha ocurrido un error');
        }
});

}
 
function generarPDF(id){
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
    }
   }); 
$.ajax({
url:"/api/empleados/"+id+"/sueldo",
type:'get',
dataType:"json",
success:function(data)
{
var sueldo_neto,sueldo_bruto,bono,isr,myNumeral;
sueldo_neto=data["sueldo_neto"];
sueldo_bruto=data["sueldo_bruto"];
bono=data["bono"];
isr=data["isr"];
sueldo_neto=``+sueldo_neto+``;
sueldo_bruto=``+sueldo_bruto+``;
bono=``+bono+``;
isr=``+isr+``
myNumeral = numeral (isr);
isr = myNumeral.format('$0.00');
var doc = new jsPDF();
doc.setFontSize(20);
doc.text(85, 30, 'Nomina Rinku');

doc.setFontSize(12);
doc.text(10, 40, 'Recibi de Rinku la cantidad de '+ sueldo_neto + 
" por concepto de mi sueldo del mes como se detalla a");
doc.text(10, 45, 'continuacion.');


doc.setFontSize(14);
doc.text(10, 70, 'Conceptos');
doc.text(10, 80, 'SUELDO');
doc.text(10, 90, 'ISR');
doc.text(10, 100, 'A PAGAR');


doc.setFontSize(14);
doc.text(90, 70, 'Ingresos');
doc.text(90, 80, sueldo_bruto);
doc.text(90, 100, sueldo_neto);



doc.setFontSize(14);
doc.text(170, 70, 'Egresos');
doc.text(175, 90, isr);





doc.save('Nomina.pdf');
    
},
statusCode: {
    404: function() {
       alert('web not found');
    }
 },
 error:function(x,xs,xt){

          alert('Hubo un error al generar la nomina');
      }
    })


}
