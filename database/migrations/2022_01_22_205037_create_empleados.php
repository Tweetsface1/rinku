<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('empleado');
            $table->string('nombre1',30);
            $table->string('nombre2',30)->nullable();
            $table->string('apellido_paterno',30);
            $table->string('apellido_materno',30)->nullable();
            $table->tinyInteger('rol')->references('id')->on('rol_empleados');
            $table->boolean('tipo_empleado')->references('id')->on('tipo_empleados');
            $table->float('sueldo_base')->default(30.00);
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
