<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Entregas</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{ asset('js/funciones.js') }}"></script>
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/empleados">Rinku</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ">
      <a class="nav-item nav-link active" href="/empleados">Empleados</a>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="/entregas/consulta" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Movimientos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/entregas/consulta">Consulta de Movimientos</a>
          <a class="dropdown-item" href="/entregas">Capturar Movimientos</a>
        </div>
      </li>
      <a class="nav-item nav-link active" href="/logout" ><span class="navbar-text">
     Cerrar Sesion
    </span></a>
     
    </div>
  </div>
</nav>
    </header>
    <div class="container-fluid">
    <h2>Consulta de Movimientos</h2>
    <h2><input type="text" id="buscador_movimientos" name="buscador_movimientos" class="form-control" placeholder="Buscar por numero de empleado">
    </h2>
    </div>
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modificar Cantidad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container">
  <div class="row">
    <div class="col-sm">
    <input type="hidden" id="id_entrega" name="id_entrega" class="form-control" placeholder="Primer Nombre">
    <input type="number" id="cantidad" name="cantidad" class="form-control" placeholder="Cantidad de Entregas">
    </div>
  </div>
  <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-outline-success" onClick="fnUpdateCantidad();">Guardar Cambios</button>
      </div>
  
  </div>
</div>
    </div>
  </div>
</div>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
    <div class="table-responsive">
<table id="myTable" class="table">
  <thead>
    <tr>
      <th scope="col">No.Empleado</th>
      <th scope="col">Nombre</th>
      <th scope="col">Cantidad de Entregas</th>
      <th scope="col">Cubrio Turno</th>
      <th scope="col">Fecha de Entrega</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody  id="entregas"> 
  </tbody>
</div>
</table>
</div>
    
</body>
</html>