<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Entregas</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{ asset('js/funciones.js') }}"></script>
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/empleados">Rinku</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ">
      <a class="nav-item nav-link active" href="/empleados">Empleados</a>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Movimentos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/entregas/consulta">Consultar Movimiento</a>  
        </div>
      </li>
      <a class="nav-item nav-link active" href="/logout" ><span class="navbar-text">
     Cerrar Sesion
    </span></a>
     
    </div>
  </div>
</nav>
    </header>
    <div class="container-fluid">
    <h2>Captura de movimientos</h2>
    <h2><input type="text" id="buscador_empleado" name="buscador_empleado" class="form-control" placeholder="Buscar Empleado">
    </h2>
        <div class="container-fluid">
          <div class="row">
             <div class="col-sm-3">
             <input type="hidden" id="id_entregas" name="id_entregas" class="form-control" >
               <input type="text" id="nombre1_entregas" name="nombre1_entregas" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="nombre2_entregas" name="nombre2_entregas" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="apellido_paterno_entregas" name="apellido_paterno_entregas" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="apellido_materno_entregas" name="apellido_materno_entregas" class="form-control" readonly> 
             </div>
             <div class="row d-flex justify-content-center">
            
             <div class="col-sm-3">
             <input type="hidden" id="rol_oculto" name="rol_oculto" class="form-control" >
               <input type="text" id="rol_entregas" name="rol_entregas" class="form-control" readonly>
             </div>

             <div class="col-sm-3">
               <input type="text" id="tipo_entregas" name="tipo_entregas" class="form-control" readonly>
             </div>

             <div class="col-sm-3">
             <input type="date" id="fecha_entregas" name="fecha_entregas" class="form-control" readonly>
             </div>

          </div>
          <div class="col-sm-12 d-flex justify-content-center">
          <div class="col-sm-2 text-center">
         <label for="cantidad_entregas">Numero de Entregas<input type="text" id="cantidad_entregas" name="cantidad_entregas" class="form-control" ></label>
           </div>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">

          
          <div class="col-sm-12">
          <div class="card">
          <div class="card-header">Cubrir turno</div>
            <div class="card-body">
               <blockquote class="blockquote mb-0">
                <div class="row justify-content-center">
                <div class="col-sm-4">
                 <input type="text" id="buscador_suplido" name="buscador_suplido" class="form-control" placeholder="Buscar Empleado">
                </div>
                <div class="row">
             <div class="col-sm-3">
              <input type="hidden" id="id_suplido" name="id_suplido" class="form-control"> 
               <input type="text" id="nombre1_suplido" name="nombre1_suplido" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="nombre2_suplido" name="nombre2_suplido" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="apellido_paterno_suplido" name="apellido_paterno_suplido" class="form-control" readonly>
             </div>
             <div class="col-sm-3">
               <input type="text" id="apellido_materno_suplido" name="apellido_materno_suplido" class="form-control" readonly> 
             </div>
             <div class="row d-flex justify-content-center">
            
             <div class="col-sm-3">
               <input type="text" id="rol_suplido" name="rol_suplido" class="form-control" readonly>
             </div>

             <div class="col-sm-3">
               <input type="text" id="tipo_suplido" name="tipo_suplido" class="form-control" readonly>
             </div>
</div>
               </div>
                </blockquote>
             </div>
           </div>
           <div class="d-flex justify-content-center">
               <button id="btnmov" name="btnmov" type="button"   class="btn btn-outline-success btn-lg btncreate" onClick="fnCreateMov();">Guardar Movimientos</button>
             </div>
            </div>
           </div>

        </div>
    </div>
    
</body>
</html>