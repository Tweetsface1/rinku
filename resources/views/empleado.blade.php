<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="{{ asset('js/funciones.js') }}"></script>
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <title>Empleados</title>
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/empleados">Rinku</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ">
      <a class="nav-item nav-link active" href="/empleados">Empleados</a>
      <a class="nav-item nav-link active" href="/entregas">Movimientos</a>
      <a class="nav-item nav-link active" href="/logout" ><span class="navbar-text">
     Cerrar Sesion
    </span></a>
    </div>
  </div>
</nav>
    </header>
    <div class="container-fluid">
    <h2>Catalogo de Empleados</h2>
    <h2> <input type="text" id="buscador" name="buscador" class="form-control" placeholder="Buscar Empleado">
    <button class="btn btn-outline-light btn-sm" data-toggle="modal" data-target="#exampleModal"><img src="img/agregar-usuario.png" width="30"></button>
    </h2>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container">
  <div class="row">
    <div class="col-sm">
    <input type="text" id="nombre1" name="nombre1" class="form-control" placeholder="Primer Nombre">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="nombre2" name="nombre2" class="form-control" placeholder="Segundo Nombre">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="apellido_paterno" name="apellido_paterno" class="form-control" placeholder="Apellido Paterno">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="apellido_materno" name="apelido_materno" class="form-control" placeholder="Apellido Materno">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <select class="form-control" id="rol" name="rol">
    
    </select>
    </div>
    <div class="col-sm">
    <select class="form-control"  id="tipo_empleado" name="tipo_empleado">
       
    </select>
    </div>
  </div>
</div>
    
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-outline-success" onClick="fnCreate();">Guardar Datos</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modificar Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container">
  <div class="row">
    <div class="col-sm">
    <input type="hidden" id="empleado" name="empleado" class="form-control" placeholder="Primer Nombre">
    <input type="text" id="nombre1_edit" name="nombre1_edit" class="form-control" placeholder="Primer Nombre">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="nombre2_edit" name="nombre2_edit" class="form-control" placeholder="Segundo Nombre">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="apellido_paterno_edit" name="apellido_paterno_edit" class="form-control" placeholder="Apellido Paterno">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <input type="text" id="apellido_materno_edit" name="apelido_materno_edit" class="form-control" placeholder="Apellido Materno">
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
    <select class="form-control" id="rol_edit" name="rol_edit">
    
    </select>
    </div>
    <div class="col-sm">
    <select class="form-control"  id="tipo_empleado_edit" name="tipo_empleado_edit">
       
    </select>
    </div>
  </div>
</div>
    
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-outline-success" onClick="fnUpdate();">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>
<div class="table-responsive">
<table id="myTable" class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Rol</th>
      <th scope="col">Tipo de Empleado</th>
      <th scope="col">Sueldo Base</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody  id="cuerpo"> 
  </tbody>
</table>
</div>
    </div>

</body>
</html>